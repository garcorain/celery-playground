# Python Celery playground

## About
Ticketing system using Flask, Celery, RabbitMQ and MySQL.

## Usage

```
docker-compose up
```

* Get http://localhost:8080
