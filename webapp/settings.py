import os


API_VERSION = "v1"

MYSQL = {
    "HOST": os.environ["MYSQL_HOST"],
    "USER": os.environ["MYSQL_USER"],
    "PASS": os.environ["MYSQL_PASSWORD"],
    "DB": os.environ["MYSQL_DATABASE"]
}

RABBITMQ = {
    "HOST": os.environ["RABBITMQ_HOST"],
    "USER": os.environ["RABBITMQ_DEFAULT_USER"],
    "PASS": os.environ["RABBITMQ_DEFAULT_PASS"],
    "VHOST": os.environ["RABBITMQ_DEFAULT_VHOST"],
}

DB_URI = "mysql://{}:{}@{}/{}".format(MYSQL["USER"], MYSQL["PASS"], MYSQL["HOST"], MYSQL["DB"])
AMQP_URI = "amqp://{}:{}@{}{}".format(RABBITMQ["USER"], RABBITMQ["PASS"], RABBITMQ["HOST"], RABBITMQ["VHOST"])

NUM_TICKETS = 100
TICKET_STATUS = ["pending", "completed"]

APP_DEBUG = True
APP_SECRET_KEY = "super_secret_key"
APP_PORT = 5000
