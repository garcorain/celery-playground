from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime
from database import Base


class Ticket(Base):
    __tablename__ = 'tickets'
    uuid = Column(Integer, primary_key=True)
    random_name = Column(String(32), nullable=False)
    status = Column(String(32), nullable=False)
    timestamp = Column(DateTime, default=datetime.utcnow)

    @property
    def serialize(self):
        return {
            "uuid": self.uuid,
            "random_name": self.random_name,
            "status": self.status,
            "timestamp": self.timestamp
        }
