from flask import Flask, render_template, request, redirect, url_for, jsonify
from celery import Celery, states
from database import init_db, db_session
from models import Ticket
from sqlalchemy.orm.exc import NoResultFound
from settings import API_VERSION, APP_DEBUG, APP_SECRET_KEY, APP_PORT
from settings import AMQP_URI, NUM_TICKETS, TICKET_STATUS
import time
import random
import string

init_db()

app = Flask(__name__)

app.url_map.strict_slashes = False

app.config["CELERY_BROKER_URL"] = AMQP_URI
app.config["CELERY_BACKEND"] = AMQP_URI

celery = Celery(app.name, broker=AMQP_URI, backend=AMQP_URI)
celery.conf.update(app.config)


@app.before_first_request
def produceTickets():
    for i in range(NUM_TICKETS):
        ticket = Ticket(
            random_name="".join(random.choices(string.ascii_lowercase, k=8)),
            status=random.choice(TICKET_STATUS)
        )
        db_session.add(ticket)
        db_session.commit()


@app.errorhandler(400)
def badRequest(error=None):
    message = {
        "status": 400,
        "message": "Bad Request: {}".format(request.body),
    }
    response = jsonify(message)
    response.status_code = 400
    return response


@app.errorhandler(404)
def notFound(error=None):
    message = {
        "status": 404,
        "message": "Not Found: {}".format(request.url),
    }
    response = jsonify(message)
    response.status_code = 404
    return response


@app.errorhandler(405)
def methodNotAllowed(error=None):
    message = {
        "status": 405,
        "message": "Method Not Allowed: {}".format(request.method),
    }
    response = jsonify(message)
    response.status_code = 405
    return response


@app.route("/api/{}".format(API_VERSION))
def rootJSON():
    return notFound()


@app.route("/api/{}/tickets/".format(API_VERSION), methods=["GET"])
def ticketListJSON():
    if request.method != "GET":
        return methodNotAllowed()

    tickets = db_session.query(Ticket).all()
    db_session.commit()
    return jsonify(tickets=[t.serialize for t in tickets])


@app.route("/api/{}/tickets/<int:ticket_uuid>".format(API_VERSION), methods=["GET"])
def ticketJSON(ticket_uuid):
    if request.method != "GET":
        return methodNotAllowed()

    try:
        ticket = db_session.query(Ticket).get(ticket_uuid)
        db_session.commit()
        if not ticket:
            raise NoResultFound
    except NoResultFound:
        return notFound()

    return jsonify(ticket=ticket.serialize)


@app.route("/api/{}/tickets/add".format(API_VERSION), methods=["POST"])
def addTicketJSON():
    if request.method != "POST":
        return methodNotAllowed()

    if not request.json["status"] or request.json["status"] not in TICKET_STATUS:
        return badRequest()

    newTicket = Ticket(
        random_name="".join(random.choices(string.ascii_lowercase, k=8)),
        status=request.json["status"]
    )
    db_session.add(newTicket)
    db_session.commit()
    response = jsonify(ticket=newTicket.serialize)
    response.status_code = 201
    return response


@app.route("/api/{}/tickets/collect".format(API_VERSION), methods=["POST"])
def collectTicketJSON():
    if request.method != "POST":
        return methodNotAllowed()

    if len(request.data) != 0:
        return badRequest()

    myTicket = db_session.query(Ticket).filter_by(status="pending").first()
    db_session.commit()
    ticket_uuid = myTicket.uuid
    ticketTask = updateTicket.apply_async(args=[ticket_uuid])
    return jsonify(ticket=myTicket.serialize, task_id=ticketTask.task_id)


@app.route("/")
@app.route("/tickets/")
def ticketList():
    tickets = db_session.query(Ticket).all()
    db_session.commit()
    return render_template("ticketlist.html", tickets=tickets)


@app.route("/tickets/<int:ticket_uuid>/")
def ticket(ticket_uuid):
    try:
        ticket = db_session.query(Ticket).get(ticket_uuid)
        db_session.commit()
        if not ticket:
            raise NoResultFound
    except NoResultFound:
        return render_template("404.html", url=request.url), 404
    return render_template("ticket.html", ticket=ticket)


@app.route("/tickets/add", methods=["GET", "POST"])
def addTicket():
    if request.method == "POST":
        newTicket = Ticket(
            random_name="".join(random.choices(string.ascii_lowercase, k=8)),
            status=request.form["status"]
        )
        db_session.add(newTicket)
        db_session.commit()
        return redirect(url_for("ticketList"))
    else:
        return render_template("addticket.html")


@app.route("/tickets/collect", methods=["GET", "POST"])
def collectTicket():
    myTicket = db_session.query(Ticket).filter_by(status="pending").first()
    db_session.commit()
    if request.method == "POST":
        ticket_uuid = myTicket.uuid
        ticketTask = updateTicket.apply_async(args=[ticket_uuid])
        return render_template("task.html", task_id=ticketTask.task_id, ticket=myTicket)
    else:
        return render_template("collectticket.html", ticket=myTicket)


@celery.task(bind=True)
def updateTicket(self, ticket_uuid):
    self.update_state(state=states.PENDING)
    ticket = db_session.query(Ticket).get(ticket_uuid)
    ticket_random_name = ticket.random_name
    ticket.status = "completed"
    time.sleep(3)
    db_session.commit()
    return "ticket {} updated".format(ticket_random_name)


if __name__ == "__main__":
    app.secret_key = APP_SECRET_KEY
    app.debug = APP_DEBUG
    app.run(host="0.0.0.0", port=APP_PORT)
